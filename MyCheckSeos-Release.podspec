Pod::Spec.new do |s|
  s.name             = "MyCheckSeos-Release"
  s.version          = "1.0.0"
  s.summary          = "Open readers with your iOS device"
  s.homepage         = "https://www.bitbucket.org/mycheck/mycheckseos.git"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }

  s.author           = { 'Dudi hisine' => 'dudih@mycheck.co.il' }
  s.source = { :git => 'https://www.bitbucket.org/mycheck/mycheckseos.git', :tag => s.version.to_s }

  s.requires_arc = true

  s.ios.deployment_target = '11.0'
  s.ios.frameworks = 'Foundation', 'CoreTelephony', 'Security', 'CoreLocation', 'CoreBluetooth', 'CoreMotion', 'UIKit', 'SystemConfiguration', 'LocalAuthentication'

  s.watchos.deployment_target = '4.0'
  s.watchos.frameworks = 'Foundation', 'Security', 'CoreLocation', 'CoreBluetooth', 'CoreMotion', 'UIKit'
  s.swift_versions = '5.0'
  
  s.module_name = 'MyCheckSeosSDK'

  s.dependency 'JSONModel', '~> 1.7.0'
  s.dependency 'CocoaLumberjack', '~> 3.2.1'
  s.dependency 'Mixpanel', '~> 3.3.3'
  s.dependency 'BerTlv', '~> 0.2.3'
  
  s.source_files = 'MyCheckKeys/Class/**/*','Frameworks/license.plist'
  s.vendored_frameworks = 'Frameworks/Release/SeosMobileKeysSDK.framework'
  

end
