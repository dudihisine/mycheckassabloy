//
//  MobileKeysTypesExtensions.swift
//  MyCheckAssabloy
//
//  Created by Dudi Hisine on 28/05/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import Foundation
import SeosMobileKeysSDK

extension MobileKeysOpeningType{
    func toString()->String{
        switch self {
        case .proximity:
            return "Proximity"
        case .motion:
            return "Motion"
        case .seamless:
            return "Seamless"
        case .applicationSpecific:
            return "App Specific"
        case .enhancedTap:
            return "Enhanced Tap"
        default:
            return "Unknown"
        }
    }
}

extension MobileKeysOpeningStatusType{
    func toString()->String{
        switch self {
        case .success:
            return "Success"
        case .timedOut:
            return "Timed out"
        case .outOfRange:
            return "Out of range"
        case .busy:
            return "Busy"
        case .motionNotSupported:
            return "Motion not supported"
        default:
            return "Unknown status"
        }
    }
}
