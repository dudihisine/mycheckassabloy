//
//  MyCheckKeysAssabloy.swift
//  MyCheckAssabloy
//
//  Created by Dudi Hisine on 28/05/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import MyCheckKeysCore
import MyCheckUtils
import SeosMobileKeysSDK

public class DormakabaProvider : KeysProvider{
    private var assabloyKeys = MyCheckKeysAssabloy()

    public func get() -> MyCheckKeys {
        return assabloyKeys
    }
}

class MyCheckKeysAssabloy: AbstructMyCheckKeys {

    internal var mManager : AssabloyInteractor?
    internal var config : MKConfig?
    internal var availableRooms : [MKRoom]?

    override func initSDK(config: MKConfig) {
        mManager = AssabloyInteractor(with: self, debugModeEnabled: config.debugMode)

        mManager?.start()

        sendDevicesRequest(delegate: self)
    }

    override func getConfig() -> MKConfig? {
        return config
    }

    override func activateRoom(room: MKRoom, maxActivationTimeInSec: TimeInterval) {
        switch checkBTPermission() {
        case .notDetermined,.unknown:
            askForBTPermission()
            return
        case .allowedAlways,.restricted:
            //continue
            break
        default: //denied or uknown
            showSettingsAlert(title: "App need BT permissions", message: "please go to settings and enable BT permissions") { [weak self] in
                guard let self = self else{ return }

                self.onFailure(error: NSError(domain: "permission denied", code: 99))
            }
            return
        }
    }

    override func getAllAvailableRooms() ->[MKRoom]{
        return availableRooms ?? []
    }

    override func deactivateAllKeys(){
        //TODO: deactivate
    }

    override func unregisterUser() {
        mManager?.terminateEndpoint()
    }

    override func didFinishRequestWithData(data: MKDevicesData?) {
        if let data = data, data.status, let token = data.token, let externalDeviceId = data.externalDeviceId{
            register(token: token)
        }else{
            didFinishWithError(error: NSError(domain: "Cant find params", code: -1))
        }
    }

    override func didFinishWithError(error: Error) {
        self.onFailure(error: error as NSError)
    }
}

extension MyCheckKeysAssabloy{
    func register(token: String){
        if isValidCode(token: token){
            //register
            mManager?.register(code: token)
            return
        }

        self.onFailure(error: NSError(domain: "token is not in the correct format", code: -1))
    }

    func isValidCode(token: String) -> Bool {
        let validPattern = "^[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}$"
        var regex: NSRegularExpression? = nil
        do {
            regex = try NSRegularExpression(pattern: validPattern, options: [])
        } catch {
        }

        return regex?.firstMatch(in: token, options: [], range: NSRange(location: 0, length: token.count)) != nil
    }

}

extension MyCheckKeysAssabloy: AssabloyInteractorDelegate{
    func endRefreshing() {
        //TODO: What?
    }

    func setKeys(keys: [MobileKeysKey]?, keyTags: [AnyHashable : Any]?) {

    }

    func parseRoom(with index : Int,from metadata : MobileKeysKey) ->MKRoom?{
        let room = MKRoom()

        //insert data to room

        room.index = index

        var shouldReturnRoom = false

//        if let reservationNumber = metadata.{
//            room.reservationNumber = reservationNumber
//            shouldReturnRoom = true
//        }
        if let checkoutDateUTC = metadata.endDate{
            room.checkoutDateUTC = checkoutDateUTC
            shouldReturnRoom = true
        }
//        if let roomNumber = metadata["RoomNumber"]?.getStringValue(){
//            room.roomNumber = roomNumber
//            shouldReturnRoom = true
//        }
        if let checkinDate = metadata.beginDate{
            room.checkinDate = checkinDate
            shouldReturnRoom = true
        }
//        if let timeStampUTC = metadata["TimeStampUTC"]?.getStringValue(){
//            room.timeStampUTC = timeStampUTC
//            shouldReturnRoom = true
//        }

        if shouldReturnRoom {
            return room
        }

        return nil
    }

    func setSeosIdLabel(seosId: String) {

    }

    func onError(error: NSError) {
        onFailure(error: error)
    }

    func onBTOffOrNotAvaliable() {
        switch checkBTPermission() {
        case .notDetermined,.unknown:
            askForBTPermission()
            return
        case .allowedAlways,.restricted:
            //continue
            break
        default: //denied or uknown
            showSettingsAlert(title: "App need BT permissions", message: "please go to settings and enable BT permissions") { [weak self] in
                guard let self = self else{ return }

                self.self.onFailure(error: NSError(domain: "permission denied", code: 99))
            }
            return
        }
    }
}
