//
//  AssabloyInteractor.swift
//  MyCheckAssabloy
//
//  Created by Dudi Hisine on 28/05/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

import AudioToolbox
import CoreLocation
import Foundation
import SeosMobileKeysSDK

let APPLICATION_ID = "EXAMPLE"
let LOCK_SERVICE_CODE_AAMK = 1
let LOCK_SERVICE_CODE_HID = 2
private let minTimeBetweenConnections: TimeInterval = 1.0 // Minimum time between buzz/sound

protocol AssabloyInteractorDelegate {
    func endRefreshing()
    func setKeys(keys: [MobileKeysKey]?, keyTags: [AnyHashable : Any]?)
    func setSeosIdLabel(seosId: String)
    func onError(error: NSError)
    func onBTOffOrNotAvaliable()
}

class AssabloyInteractor: NSObject {
    private var applicationIsStarting = false
    private var lockServiceCodes: [AnyHashable]?
    private var seamlessOn = false
    private var openingModesWithSeamless: [MobileKeysOpeningType]?
    private var openingModesWithoutSeamless: [MobileKeysOpeningType]?
    private var currentlyEnabledOpeningModes: [MobileKeysOpeningType]?
    private var delegate: AssabloyInteractorDelegate!
    private var debugModeEnabled: Bool = false

    private var mobileKeysManager: MobileKeysManager?
    private var mobileKeys: [MobileKeysKey]?
    private var keyTags: [AnyHashable : Any]?
    private var seosId = 0
    private var locationManager: CLLocationManager?
    private var timeOfLastConnection: Date?

    init(with delegate: AssabloyInteractorDelegate, debugModeEnabled: Bool) {
        super.init()
        // Place delegate
        self.delegate = delegate
        // Place debugModeEnabled
        self.debugModeEnabled = debugModeEnabled
        // Create the MobileKeysManager
        mobileKeysManager = createInitializedMobileKeysManager()
        // The location manager is used to ask the user for permission to use location services.
        locationManager = CLLocationManager()
        // Lock service codes are used to specify what readers the Manager should scan for
        lockServiceCodes = [NSNumber(value: LOCK_SERVICE_CODE_AAMK), NSNumber(value: LOCK_SERVICE_CODE_HID)]
        // By the default, the lock will not use seamless mode
        seamlessOn = false
        // Used for calculating time between vibration alerts
        timeOfLastConnection = Date(timeIntervalSince1970: 1.0)
        // A list of opening modes without seamless
        openingModesWithoutSeamless = [MobileKeysOpeningType.motion, MobileKeysOpeningType.proximity, MobileKeysOpeningType.applicationSpecific, MobileKeysOpeningType.enhancedTap]
        // A list of opening modes with seamless
        openingModesWithSeamless = [MobileKeysOpeningType.motion, MobileKeysOpeningType.proximity, MobileKeysOpeningType.applicationSpecific, MobileKeysOpeningType.enhancedTap]
        // Opening modes enabled on startup
        currentlyEnabledOpeningModes = openingModesWithoutSeamless
    }

    func start(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnteredBackground), name: .NSExtensionHostDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleDidBecomeActive), name: .NSExtensionHostDidEnterBackground, object: nil)

        self.printIfDebug("Using SDK version \(mobileKeysManager?.apiVersion() ?? "")")
        if isEndpointSetup(){
            self.printIfDebug("Personalized and showing KeyView")
            applicationIsStarting = true;
            self.printIfDebug("Starting application")
            cacheSeosId()
            toggleScanning()
            showKeyView()

            if CLLocationManager.authorizationStatus() == .authorizedAlways{
                locationManager?.allowsBackgroundLocationUpdates = true
                self.printIfDebug("allowsBackgroundLocationUpdates")
            }
        }else{
            self.printIfDebug("Not Personalized, showing PersonalizationView")
            showRegistrationView()
        }

        mobileKeysManager?.startup()
    }

    func createInitializedMobileKeysManager()->MobileKeysManager{
        var version = "\(APPLICATION_ID)"
        if let info = Bundle.main.infoDictionary,
            let shortVersion = info["CFBundleShortVersionString"] as? String,
            let longVersion = info["CFBundleVersion"] as? String{
            version = "\(version)-\(shortVersion) \(longVersion)"
        }

        let config = [MobileKeysOptionApplicationId:APPLICATION_ID,MobileKeysOptionVersion:version]

        return MobileKeysManager(delegate: self, options: config)
    }

    func handleError(error: NSError?){
        if let error = error{
            self.printIfDebug("ERROR: \(error)")
            delegate.onError(error: error)
        }
    }

    //MARK: Mobile Keys API method example implementation

    func isEndpointSetup()->Bool{
        do{
            try mobileKeysManager?.isEndpointSetup()
            return true
        }catch{
            handleError(error: error as NSError)
            return false
        }
    }

    func register(code: String){
        self.printIfDebug("Personalizing...")
        mobileKeysManager?.setupEndpoint(code)
    }

    func setupEndpoint(){
        if isEndpointSetup(){
            self.printIfDebug("Updating EndPoint")
            mobileKeysManager?.updateEndpoint()
        }
    }

    func getKeysFromSeos(){
        var error : NSError?
        if let keys = mobileKeysManager?.listMobileKeys(&error){
            mobileKeys = keys
            return
        }

        handleError(error: error)
    }

    func cacheSeosId(){
        var error : NSError?
        if let endpointInfo = mobileKeysManager?.endpointInfo(&error){
            seosId = Int(endpointInfo.seosId)
            self.printIfDebug("Cached Seos ID: \(Int(endpointInfo.seosId))")
            return
        }
    }

    //MARK: Location manager

    @objc func handleDidBecomeActive(){
        if CLLocationManager.authorizationStatus() == .authorizedAlways{
            locationManager?.startUpdatingLocation()
            self.printIfDebug("startUpdatingLocatio")
        }
    }

    @objc func handleEnteredBackground(){
        getKeysFromSeos()

        if mobileKeys?.count ?? 0 > 0{
            locationManager?.requestAlwaysAuthorization()
        }
    }

    //MARK: Mobile Keys Bluetooth API example implementation

    func toggleScanning(openingTypes : [Any]? = nil, scanMode: MobileKeysScanMode? = nil){
       var listKeysError : NSError?
        if mobileKeysManager?.listMobileKeys(&listKeysError).count ?? 0 > 0{
            if mobileKeysManager?.isScanning() ?? false{
                self.printIfDebug("Restart scanning")
                mobileKeysManager?.stopReaderScan()
            }else{
                self.printIfDebug("Start scanning")
            }

            var error : NSError?
            if let scanMode = scanMode, let openingTypes = openingTypes, let lockServiceCodes = lockServiceCodes, mobileKeysManager?.listMobileKeys(&error).count ?? 0 > 0{
                mobileKeysManager?.startReaderScan(in: scanMode, supportedOpeningTypes: openingTypes, lockServiceCodes: lockServiceCodes, error: &error)
            }else{
                self.printIfDebug("")
            }

            if let error = error{
                switch error.code {
                case MobileKeysErrorCode.bluetoothLENotAvailable.rawValue:
                    self.printIfDebug("Couldn't start scan due BLE being off or not available")
                    delegate.onBTOffOrNotAvaliable()
                    break
                default:
                    handleError(error: error)
                    break
                }
            }
        }else{
            self.printIfDebug("No keys - stopped scanning")
            mobileKeysManager?.stopReaderScan()
            handleError(error: listKeysError)
        }
    }

    func listReaders(){
        if let readers = mobileKeysManager?.listReaders(){
            self.printIfDebug("All readers (\(readers.count))")
            for reader in readers{
                let enhancedTapSupported = reader.supportsOpeningType(.enhancedTap)
                let proxSupported = reader.supportsOpeningType(.proximity)
                let motionSupported = reader.supportsOpeningType(.motion)
                let applicationSupported = reader.supportsOpeningType(.applicationSpecific)

                let message = "uuid: \(shortenReaderUid(reader: reader)) rssi: \(reader.meanRssi()) \(enhancedTapSupported ? "E" : "")\(proxSupported ? "P" : "")\(motionSupported ? "M" : "")\(applicationSupported ? "A" : "")"

                self.printIfDebug(message)
            }
        }
        self.printIfDebug("E = (Enhanced Tap supported)")
        self.printIfDebug("P = (Proximity supported)")
        self.printIfDebug("M = (Motion supported)")
        self.printIfDebug("A = (Application specific supported)")
    }

    func healthCheck(){
        if let healthCheck = mobileKeysManager?.healthCheck() as? [NSNumber]{
            for mkit in healthCheck{
                if let type = MobileKeysInfoType(rawValue: mkit.intValue){
                    switch type {
                    case .bleNotSupported:
                        self.printIfDebug("BLE not supported")
                        break
                    case .bleTurnedOff:
                        self.printIfDebug("BLE turned off")
                        break
                    case .locationServicesNotEnabled:
                        self.printIfDebug("Location services not enabledd")
                        break
                    case .locationServicesNotDetermined:
                        // For this to work you need the to add this to your AppName-Info.plist
                        // <key>NSLocationAlwaysUsageDescription</key>
                        // <string>Location is required to improve scanning performance</string>
                        // You can localize this.
                        self.printIfDebug("Location services not determined")
                        locationManager?.requestAlwaysAuthorization()
                        break
                    case .locationServicesTurnedOff:
                        self.printIfDebug("Location services turned off")
                        break
                    case .locationMonitoringNotSupported:
                        self.printIfDebug("Location monitoring not supported")
                        break
                    case .locationMonitoringTurnedOff:
                        self.printIfDebug("Location monitoring turned off")
                        break
                    case .passcodeWarning:
                        self.printIfDebug("Passcode may not be set")
                        break
                    case .bleSharingTurnedOff:
                        self.printIfDebug("BLE Sharing Turned off. Tap will not work with Readers that support Enhanced Tap.")
                        break
                    @unknown default:
                        break
                    }
                }
            }
        }
    }

    //MARK: View handling

    func showRegistrationView(){

    }

    func showKeyView(){
        reloadKeys()
        delegate.setSeosIdLabel(seosId: String(describing: seosId))
    }

    func openClosetsReader(){
        if let reader = mobileKeysManager?.closestReader(withinRangeOf: .applicationSpecific){
            var error : NSError?
            self.printIfDebug("Opening reader: \(shortenReaderUid(reader: reader))")
            mobileKeysManager?.connect(to: reader, openingType: .applicationSpecific, error: &error)
            if let error = error{
                self.printIfDebug("Error calling connectoToReader: \(error)")
                handleError(error: error)
            }
        }else{
            self.printIfDebug("No reader in range")
            handleError(error: NSError(domain: "No reader in range", code: 1))
        }
    }

    func refreshKeyView(){
        setupEndpoint()
    }

    func reloadKeys(){
        getKeysFromSeos()
        delegate.setKeys(keys: mobileKeys, keyTags: keyTags)
    }

    func didToggleSeamless(){
        if seamlessOn{
            currentlyEnabledOpeningModes = openingModesWithSeamless
            seamlessOn = false
        }else{
            currentlyEnabledOpeningModes = openingModesWithoutSeamless
            seamlessOn = true
        }

        toggleScanning(openingTypes: currentlyEnabledOpeningModes, scanMode: .optimizePerformance)
    }

    //MARK: User feedback

    func isForeground()->Bool{
        UIApplication.shared.applicationState == .active
    }

    func shouldVibrateWhenConnect()->Bool{
        return true
    }

    func shouldPlaySoundWhenConnect()->Bool{
        return true
    }

    func playSound(){

    }

    func giveConnectionFeedbackToUser(){
        if isForeground(){
            if shouldVibrateWhenConnect(){
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
            if shouldPlaySoundWhenConnect(){
                playSound()
            }
        }
    }

    //MARK: Logging

    func printIfDebug(_ message : String){
        if debugModeEnabled{
            print(message)
        }
    }

    func constructLogMessage(reader: MobileKeysReader?, withOpeningType type: MobileKeysOpeningType, openingStatus status: MobileKeysOpeningStatusType)->String{
        return "Reader: \(shortenReaderUid(reader: reader)). Status: \(status.toString()). Type: \(type.toString())."
    }

    func shortenReaderUid(reader : MobileKeysReader?)->String{
        if let reader = reader{
            return String(reader.uuid.prefix(reader.uuid.count - 8))
        }
        return "NULL"
    }

    //MARK: Testing methods

    func terminateEndpoint(){
        mobileKeysManager?.unregisterEndpoint()
    }
}

//MARK: MobileKeysManagerDelegate callback methods

extension AssabloyInteractor: MobileKeysManagerDelegate{
    func mobileKeysDidStartup() {
        self.printIfDebug("mobileKeysDidStartup")
        if applicationIsStarting{
            self.printIfDebug("Application is starting. Endpoint update...")
            setupEndpoint()
            applicationIsStarting = false
        }
    }

    func mobileKeysDidFail(toStartup error: Error) {
        self.printIfDebug("mobileKeysDidFailToStartup")
        handleError(error: error as NSError)
    }

    func mobileKeysDidSetupEndpoint() {
        self.printIfDebug("mobileKeysDidSetupEndpoint")
        if !isEndpointSetup(){
            showRegistrationView()
        }
        cacheSeosId()
        showKeyView()
        delegate.endRefreshing()
        if mobileKeysManager?.isScanning() == false{
            toggleScanning(openingTypes: currentlyEnabledOpeningModes, scanMode: .optimizePerformance)
        }
    }

    func mobileKeysDidFail(toSetupEndpoint error: Error) {
        showRegistrationView()
        self.printIfDebug("Personalization failed!")
        handleError(error: error as NSError)
    }

    func mobileKeysDidUpdateEndpoint() {
        self.printIfDebug("mobileKeysDidUpdateEndpoint")
        cacheSeosId()
        showKeyView()
        delegate.endRefreshing()
        toggleScanning(openingTypes: currentlyEnabledOpeningModes, scanMode: .optimizePerformance)
    }

    func mobileKeysDidFail(toUpdateEndpoint error: Error) {
        showKeyView()
        self.printIfDebug("Endpoint Update failed")
        handleError(error: error as NSError)
        toggleScanning(openingTypes: currentlyEnabledOpeningModes, scanMode: .optimizePerformance)
    }

    func mobileKeysDidTerminateEndpoint() {
        self.printIfDebug("mobileKeysDidTerminateEndpoint")
        showRegistrationView()
    }
}

extension AssabloyInteractor{

    //MARK: Mobile Keys Bluetooth Protocol example callback handlers

    func mobileKeysDidConnect(to reader: MobileKeysReader, openingType type: MobileKeysOpeningType) {
        let logEntry = "Connected. \(constructLogMessage(reader: reader, withOpeningType: type, openingStatus: .success))"

        DispatchQueue.main.async {
            self.printIfDebug(logEntry)
        }

        if let timeOfLastConnection = timeOfLastConnection, Date().timeIntervalSince(timeOfLastConnection) > minTimeBetweenConnections{
            giveConnectionFeedbackToUser()
        }
        timeOfLastConnection = Date()
    }

    func mobileKeysDidFailToConnect(to reader: MobileKeysReader, openingType type: MobileKeysOpeningType, openingStatus status: MobileKeysOpeningStatusType) {
        let logEntry = "Failed to connect. \(constructLogMessage(reader: reader, withOpeningType: type, openingStatus: .success))"

        DispatchQueue.main.async {
            self.printIfDebug(logEntry)
        }
    }

    func mobileKeysDidDisconnect(from reader: MobileKeysReader, openingType type: MobileKeysOpeningType, openingResult result: MobileKeysOpeningResult) {
        let logEntry = "Disconnected. \(constructLogMessage(reader: reader, withOpeningType: type, openingStatus: .success))"

        DispatchQueue.main.async {
            self.printIfDebug(logEntry)
            self.printIfDebug("Disconnect payload: \(String(describing: result.statusPayload))")
        }
    }

    //MARK: Reader management example callback handlers

    func mobileKeysShouldAttempt(toOpen reader: MobileKeysReader, openingType type: MobileKeysOpeningType) -> Bool {
        return true
    }

    //MARK: Motion Detector example callback handler

    func mobileKeysUserDidUnlockGesture() {
        DispatchQueue.main.async {
            self.printIfDebug("mobileKeysDidUnlockGesture")
        }
    }
}
